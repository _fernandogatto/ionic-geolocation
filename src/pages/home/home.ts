import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  lat: any;
  lon: any;

  constructor(
    public navCtrl: NavController, 
    public geo: Geolocation,
    public loadingCtrl: LoadingController
  ) {
  }

  ionViewDidLoad() {
    const loader = this.loadingCtrl.create({
      content: "Carregando..."
    });
    loader.present();
   
    this.geo.getCurrentPosition().then(
      (data) => {
        this.lat = data.coords.latitude;
        this.lon = data.coords.longitude;
        loader.dismiss();
      }, (error) => {
        console.log(error.json());
        loader.dismiss();
      }
    );
  }

  startGps() {
    const loader = this.loadingCtrl.create({
      content: "Carregando..."
    });
    loader.present();

    this.geo.watchPosition().subscribe(
      (data) => {
        this.lat = data.coords.latitude;
        this.lon = data.coords.longitude;
        loader.dismiss();
      }, (error) => {
        console.log(error.json());
        loader.dismiss();
      }
    );
    
  }

  stopGps() {
    this.lat = null;
    this.lon = null;
  }

}
